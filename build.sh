#!/bin/sh

if [ -d "dist" ]; then
    rm -rf dist
fi

npm install

grunt build

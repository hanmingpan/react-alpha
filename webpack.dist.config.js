/*
 * Webpack distribution configuration
 *
 * This file is set up for serving the distribution version. It will be compiled to dist/ by default
 */
'use strict';

const webpack = require('webpack');
const path = require('path');

module.exports = {

    entry: {
        main: [
            './src/main.js'
        ]
    },

    output: {
        publicPath: '/assets/',
        path      : path.resolve(__dirname, 'dist/assets/'),
        filename  : '[name].[hash].js'
    },

    devtool: 'inline-source-map',

    stats: {
        colors : true,
        reasons: false
    },

    plugins: [
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 10}),
        new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000}),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: true
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin()
    ],

    resolve: {
        extensions: ['.js', '.jsx'],
        alias     : {
            'actions'   : __dirname + '/src/actions/',
            'components': __dirname + '/src/components/',
            'constants' : __dirname + '/src/constants',
            'dispatcher': __dirname + '/src/dispatcher',
            'lib'       : __dirname + '/src/lib',
            'mixins'    : __dirname + '/src/mixins',
            'stores'    : __dirname + '/src/stores/',
            'styles'    : __dirname + '/src/styles'
        }
    },

    module: {
        rules: [{
            test   : /\.(js|jsx)$/,
            enforce: "pre",
            exclude: /node_modules/,
            loader : 'jsxhint-loader'
        }, {
            test   : /\.(js|jsx)$/,
            exclude: /node_modules/,
            use    : [
                // 'react-hot-loader',
                'babel-loader'
            ]
        }, {
            test: /\.(scss|css)/,
            use : [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        localIdentName: '[name]__[local]___[hash:base64:4]',
                        sourceMap: true
                    }
                },
                'postcss-loader',
                'sass-loader'
            ]
        }, {
            test  : /\.(png|jpg|woff|woff2|ttf|svg|eot)$/,
            loader: 'url-loader?limit=8192'
        }]
    }
};

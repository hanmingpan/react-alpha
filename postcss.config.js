/**
 * Created by Hastings on 17/2/16.
 */
module.exports = {
    plugins: [
        require('precss')({ /* ...options */ }),
        require('autoprefixer')({ /* ...options */ })
    ]
};
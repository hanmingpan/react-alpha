'use strict';

var Dispatcher  = require('../dispatcher/AppDispatcher'),
    Constants   = require('../constants/AppConstants'),
    ActionTypes = Constants.ActionTypes,
    ajax        = require('./ajax');

var waitLine = {
    curNum: 0,
    waitNum: 0,
    item: []
};

var resetWaitLine = function () {
    waitLine.curNum = 0;
    waitLine.waitNum = 0;
    waitLine.item = [];
};

var statusHandler = function (data, status, options) {

    switch (status) {
        case 0:
        case 200:
            Dispatcher.handleServerAction({
                type   : options.type + '_RESPONSE',
                data   : data,
                options: options,
                status : status,
                error  : false
            });
            break;
        case 401:
            Dispatcher.handleServerAction({
                type   : ActionTypes.NEED_LOGIN,
                data   : data,
                options: options,
                status : status,
                error  : true
            });
            break;
        default:
            Dispatcher.handleServerAction({
                type   : options.type + '_RESPONSE',
                data   : data,
                options: options,
                status : status,
                error  : true
            });
            break;
    }

    if (waitLine.curNum === waitLine.item.length) {
        resetWaitLine();
    }
};


var request = function (options) {
    ++waitLine.curNum;

    Dispatcher.handleViewAction({
        type: options.type + '_REQUEST'
    });

    options.success = function (data, type, status, xhr) {
        statusHandler(data, status, options);
    };

    options.error = function (data, type, status, xhr) {
        statusHandler(data, status, options);
    };

    return ajax(options);
};

var reqLine = function() {
    return request(waitLine.item[waitLine.curNum].option);
};

var reqHandler = function (opt) {
    waitLine.item.push({
        waitNum: waitLine.waitNum,
        option: opt
    });

    setTimeout(function() {
        reqLine();
    }, waitLine.waitNum * 300);

    ++waitLine.waitNum;
};

request.get = function (option) {
    option.method = 'GET';
    return reqHandler(option);
};

request.post = function (option) {
    option.method = 'POST';
    return reqHandler(option);
};

request.put = function (option) {
    option.method = 'PUT';
    return reqHandler(option);
};

request.delete = function (option) {
    option.method = 'DELETE';
    return reqHandler(option);
};

request.fake = function (options) {

    Dispatcher.handleViewAction({
        type: options.type + '_REQUEST'
    });

    statusHandler({
        status : 0,
        message: 'OK',
        data   : options.response
    }, 200, options);

    return null;
};

module.exports = request;

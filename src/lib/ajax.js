'use strict';

var extend = function (a, b, c) {
    var k, v;

    if (typeof a !== 'object') {
        return a;
    }

    if (typeof b === 'object') {
        // merge from b to a
        for (k in b) {
            if (b.hasOwnProperty(k)) {
                a[k] = b[k];
            }
        }
    }

    if (typeof c === 'object') {
        // merge from c to a
        for (k in c) {
            if (c.hasOwnProperty(k)) {
                a[k] = c[k];
            }
        }
    }

    return a;
};

var forEach = function (list, fn) {
    var k, v;

    if (!fn) {
        return;
    }

    for (k in list) {
        if (list.hasOwnProperty(k)) {
            v = list[k];
            fn(v, k);
        }
    }
};

var ajax = function (url, options) {

    if (typeof XMLHttpRequest === 'undefined') {
        console.log('Sorry, khw ajax only support XMLHttpRequest!');
        return;
    }

    if (typeof url === 'object') {
        options = url;
        url = options.url;
    }


    var MEDIA_URL_ENCODED = 'application/x-www-form-urlencoded',
        MEDIA_JSON        = 'application/json';

    var xhr     = new XMLHttpRequest(),
        abortTimeout,
        blankRE = /^\s*$/,
        headers = {
            'Content-Type': MEDIA_JSON
        },
        accepts = {
            script: 'text/javascript, application/javascript, application/x-javascript',
            json  : 'application/json, text/javascript, */*',
            xml   : 'application/xml, text/xml',
            html  : 'text/html',
            text  : 'text/plain'
        };


    options = options || {};
    options.method = options.method.toUpperCase();
    options.dataType = options.dataType || 'json';
    options.headers = extend({}, headers, options.headers);


    var setHeader = function (name, content) {
        options.headers[name] = content;
    };


    if (options.method !== 'GET') {
        if (typeof options.data === 'object') {
            options.data = JSON.stringify(options.data);
            setHeader('Content-Type', MEDIA_JSON);
        } else {
            options.data = options.data ? options.data : null;
            setHeader('Content-Type', MEDIA_URL_ENCODED);
        }
    }

    if (!options.crossDomain) {
        setHeader('X-Requested-With', 'XMLHttpRequest');
    }

    var mime = accepts[options.dataType];
    setHeader('Accept', mime || '*/*');


    var ajaxSuccess = function (result, xhr, options) {
        if (options.success) {
            options.success.call(options.context, result, 'success', xhr.status , xhr);
        }
    };

    var ajaxError = function (type, xhr, options) {
        if (options.error) {

            var result, error = false, dataType = options.dataType;

            result = xhr.responseText;

            try {
                if (dataType === 'xml') {
                    result = xhr.responseXML;
                }
                else if (dataType === 'json') {
                    result = blankRE.test(result) ? null : JSON.parse(result);
                }
            } catch (e) {
                error = e;
            }

            options.error.call(options.context, result, type, xhr.status, xhr);
        }
    };

    var ajaxAbort = function () {
        xhr.onreadystatechange = null;
        if (abortTimeout) {
            clearTimeout(abortTimeout);
        }
    };

    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {

            ajaxAbort();

            var result, error = false, dataType = options.dataType;

            if ((xhr.status >= 200 && xhr.status < 300) ||
                xhr.status === 304) {

                result = xhr.responseText;

                try {
                    // http://perfectionkills.com/global-eval-what-are-the-options/
                    if (dataType === 'script') {
                        (1, eval)(result); // jshint ignore:line
                    }
                    else if (dataType === 'xml') {
                        result = xhr.responseXML;
                    }
                    else if (dataType === 'json') {
                        result = blankRE.test(result) ? null : JSON.parse(result);
                    }
                } catch (e) {
                    error = e;
                }

                if (error) {
                    ajaxError('parsererror', xhr, options);
                }
                else {
                    ajaxSuccess(result, xhr, options);
                }
            } else {
                ajaxError(xhr.status ? 'error' : 'abort', xhr, options);
            }
        }
    };

    if (options.timeout > 0) {
        abortTimeout = setTimeout(function () {
            ajaxAbort();
            xhr.abort();
            ajaxError('timeout', xhr, options);
        }, options.timeout);
    }

    xhr.open(options.method, url, true);
    forEach(options.headers, function (content, name) {
        xhr.setRequestHeader(name, content);
    });
    xhr.send(options.data);

    return xhr;
};


module.exports = ajax;

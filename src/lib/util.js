'use strict';

var Cookie = require('./cookie');

var util = module.exports = {
    getFullDate     : function (timeStamp) {
        var date = new Date(timeStamp);
        return date.getFullYear() + '-' + util.pad(date.getMonth() + 1) + '-' + util.pad(date.getDate()) + ' ' + util.pad(date.getHours()) + ':' + util.pad(date.getMinutes());
    },
    pad             : function (n) {
        return n > 9 ? n : '0' + n;
    },
    getLocationQuery: function () {
        var list = location.search.substr(1).split('&');
        var result = {};
        for (var i = 0, l = list.length; i < l; ++i) {
            var pair = list[i].split('=');
            result[pair[0]] = pair[1];
        }
        return result;
    },
    getWeixinId     : function () {
        var query = this.getLocationQuery();
        if (query.u) {
            return query.u;
        } else {
            return '000000';
        }
    },
    getPhone: function() {
        return Cookie.get('KHW_C__PHONE') || '';
    },
    classSet        : function (conf) {
        var list = [];
        for (var i in conf) {
            if (conf[i]) {
                list.push(i);
            }
        }
        return list.join(' ');

    },
    storage         : {
        set   : function (name, item) {
            localStorage.setItem(name, JSON.stringify(item));
        },
        get   : function (name) {
            return JSON.parse(
                localStorage.getItem(name)
            );
        },
        remove: function (name) {
            localStorage.removeItem(name);
        },
        clear : function () {
            localStorage.clear();
        }
    },
    isWeixin        : function () {
        var ua = navigator.userAgent.toLowerCase();
        return !!(/micromessenger/i.test(ua));
    },
    isIOS           : function () {
        var ua = navigator.userAgent.toLowerCase();
        var query = this.getLocationQuery();
        return (query.ua === 'KHW-IOS' || ua === 'khw-ios');
    },
    isAndroid       : function () {
        var ua = navigator.userAgent.toLowerCase();
        var query = this.getLocationQuery();
        return (query.ua === 'KHW-ANDROID' || ua === 'khw-android');
    },
    isApp           : function () {
        return (this.isIOS() || this.isAndroid());
    },
    isAndroidView: function () {
        if(/android/i.test(navigator.userAgent)){
            return true;//这是Android平台下浏览器
        }
    },
    isIOSView: function () {
        if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
            return true;//这是iOS平台下浏览器
        }
    },
    isWeiWiniew: function () {
        if(/MicroMessenger/i.test(navigator.userAgent)){
            return true;//这是iOS平台下浏览器
        }
    },
    goReg           : function (router, backPath) {
        if (!this.isWeixin()) {
            router.go('/reg?backPath=' + backPath);
        } else {
            Cookie.set('BACKPATH', backPath, {'path':'/', 'expires': 864000000});
            location.href = '/wxr/login';
        }
    }
};

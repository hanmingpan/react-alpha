'use strict';

import React from 'react';
import {Link} from 'react-router';

import Util from '../lib/util';
import style from '../styles/main.scss';


const session = {
    isInitOk: false
};

class App extends React.Component {
    constructor(props){
        super(props);

        this.state = session;
        this.updateState();
    }

    updateState(){

    }

    render() {
        var me = this;

        return (
            <div>
                {me.props.children}
            </div>
        );
    }
}

App.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

module.exports = App;

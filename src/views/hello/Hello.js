'use strict';

import React from 'react';
import {Link} from 'react-router';

import Util from '../../lib/util';
// import style from 'hello.scss';

const defaultProps = {

};

const session = {

};

class Hello extends React.Component {
    constructor(props){
        super(props);

        this.state = session;
        this.updateState();
    }

    updateState(){

    }

    render() {
        var me = this;

        return (
            <div>
                Hello2
            </div>
        );
    }
}

Hello.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

Hello.defaultProps = defaultProps;

export default Hello;

'use strict';

var keyMirror = require('keymirror');

module.exports = {

    ActionTypes: keyMirror({
        NEED_LOGIN                  : null, //需要登录
        GET_INIT                    : null, //初始化
    }),

    PayloadSources: keyMirror({
        VIEW_ACTION  : null,
        SERVER_ACTION: null
    })

};

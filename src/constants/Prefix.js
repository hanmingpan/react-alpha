'use strict';

const prefixMap = {
    SHOP: 'http://www.wangle.link'
};

const prefix = function (key) {
    return process.env.NODE_ENV === 'development' ? '' : prefixMap[key] || '';
};

module.exports = prefix;

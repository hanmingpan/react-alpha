/*global _hmt*/
"use strict";

import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory, IndexRoute} from 'react-router';

import App from './components/App';
import Hello from './views/hello/Hello';


const IndexPageRoutes = {
    Default: Hello,
};

const IndexPage = IndexPageRoutes[window.system];

const routes = (
    <Route path="/" component={App}>
        <Route path='/hello' component={Hello}/>

        <IndexRoute component={IndexPage}/>
    </Route>
);
ReactDOM.render(<Router history={hashHistory} routes={routes}/>, document.getElementById('content'));

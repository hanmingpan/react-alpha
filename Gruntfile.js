'use strict';

var serveStatic = require('serve-static');

var mountFolder = function (connect, dir) {
    return serveStatic(require('path').resolve(dir));
};

var webpackDistConfig = require('./webpack.dist.config.js'),
    webpackDevConfig  = require('./webpack.config.js');

module.exports = function (grunt) {
    // Let *load-grunt-tasks* require everything
    require('load-grunt-tasks')(grunt);

    // Read configuration from package.json
    var pkgConfig = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: pkgConfig,

        webpack: {
            options: webpackDistConfig,
            dist   : {
                cache: false
            }
        },

        'webpack-dev-server': {
            options: {
                host       : '0.0.0.0',
                hot        : true,
                port       : 8025,
                webpack    : webpackDevConfig,
                publicPath : '/assets/',
                contentBase: './<%= pkg.src %>/'
            },

            local: {
                //keepAlive: true,
                proxy    : {
                    '/user/*'     : {
                        target: 'http://localhost:9090',
                        host  : 'w.kuaihaowei.domain'
                    }
                }
            },

            online: {
                //keepAlive: true,
                proxy    : {
                    '/user/*'                    : {
                        target: 'http://10.3.0.1',
                        host  : 'w.kuaihaowei.domain'
                    }
                }
            }
        },

        connect: {
            options: {
                port: 8025
            },

            dist: {
                options: {
                    keepalive : true,
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, pkgConfig.dist)
                        ];
                    }
                }
            }
        },

        open: {
            options: {
                delay: 500
            },
            dev    : {
                path: 'http://localhost:<%= connect.options.port %>/webpack-dev-server/'
            },
            dist   : {
                path: 'http://localhost:<%= connect.options.port %>/'
            }
        },

        copy: {
            dist: {
                files: [
                    // includes files within path
                    {
                        flatten: true,
                        expand : true,
                        src    : ['<%= pkg.src %>/*'],
                        dest   : '<%= pkg.dist %>/',
                        filter : 'isFile'
                    }, {
                        flatten: true,
                        expand : true,
                        src    : ['<%= pkg.src %>/images/*'],
                        dest   : '<%= pkg.dist %>/images/'
                    }
                ]
            }
        },

        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '<%= pkg.dist %>'
                    ]
                }]
            }
        },

        replace: {
            html: {
                options: {
                    patterns: [{
                        match      : /assets\/([^\.]*)\.js/,
                        replacement: function () {
                            var name = arguments[1];
                            grunt.log.debug('find file ' + name + '.js');
                            var fileName;

                            grunt.file.recurse('dist/assets', function (abspath, rootdir, subdir, fileFile) {
                                grunt.log.debug('FindFile:', fileFile);
                                if (fileFile.indexOf(name) === 0) {
                                    fileName = fileFile;
                                }
                            });
                            grunt.log.debug('replace with', fileName);
                            return 'assets/' + fileName;
                        }
                    }, {
                        match      : /__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent\.__REACT_DEVTOOLS_GLOBAL_HOOK__/,
                        replacement: ''
                    }]
                },
                files  : [{
                    expand : true,
                    flatten: true,
                    src    : ['dist/*.html'],
                    dest   : 'dist/'
                }]
            },

            version: {
                options: {
                    patterns: [{
                        match      : 'buildTime',
                        replacement: grunt.template.date(new Date(), 'yyyy-mm-dd HH:MM:ss')
                    }]
                },
                files  : [{
                    expand : true,
                    flatten: true,
                    src    : ['dist/assets/main.*.js'],
                    dest   : 'dist/assets/'
                }, {
                    expand : true,
                    flatten: true,
                    src    : ['dist/*.html'],
                    dest   : 'dist/'
                }]
            }
        }
    });

    grunt.registerTask('serve', function (target) {

        switch (target) {
            case 'online':
                grunt.task.run([
                    'open:dev',
                    'webpack-dev-server:online'
                ]);
                break;
            case 'dist':
                grunt.task.run([
                    'build',
                    'open:dist',
                    'connect:dist'
                ]);
                break;
            default:
                grunt.task.run([
                    'open:dev',
                    'webpack-dev-server:local'
                ]);
                break;
        }
    });


    grunt.registerTask('local', ['open:dev', 'webpack-dev-server:local']);

    grunt.registerTask('build', ['clean', 'copy', 'webpack', 'replace:version', 'replace:html']);

    grunt.registerTask('default', []);
};

/*
 * Webpack development server configuration
 *
 * This file is set up for serving the webpack-dev-server, which will watch for changes and recompile as required if
 * the subfolder /webpack-dev-server/ is visited. Visiting the root will not automatically reload.
 */
'use strict';

const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {

    entry: {
        main: [
            // 'webpack/hot/only-dev-server',
            './src/main.js'
        ]
        // app: './src/main.js'
    },

    output: {
        filename  : '[name].js',
        publicPath: path.resolve(__dirname,'./assets/')
    },

    devtool: 'inline-source-map',

    devServer: {
        host: '127.0.0.1',
        port: 9006,
        contentBase: './dist/',
        publicPath: path.resolve(__dirname,'./assets/'),
        hot: true,
        hotOnly: true,
    },

    cache: true,

    stats: {
        colors : true,
        reasons: true
    },

    plugins: [
        new CleanWebpackPlugin(['dist']),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin(),
        // new webpack.NoErrorsPlugin()
    ],

    resolve: {
        extensions: ['.js', '.jsx'],
        alias     : {
            // 'actions'   : __dirname + '/src/actions/',
            // 'components': __dirname + '/src/components/',
            // 'constants' : __dirname + '/src/constants',
            // 'dispatcher': __dirname + '/src/dispatcher',
            // 'lib'       : __dirname + '/src/lib',
            // 'mixins'    : __dirname + '/src/mixins',
            // 'stores'    : __dirname + '/src/stores/',
            // 'styles'    : __dirname + '/src/styles'
        }
    },

    module: {
        rules: [{
            test   : /\.(js|jsx)$/,
            enforce: "pre",
            exclude: /node_modules/,
            loader : 'jsxhint-loader'
        }, {
            test   : /\.(js|jsx)$/,
            exclude: /node_modules/,
            use    : [
                // 'react-hot-loader',
                'babel-loader'
            ]
        },
            {
            test: /\.(scss|css)$/,
            use : [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        localIdentName: '[name]__[local]___[hash:base64:4]',
                        sourceMap: true
                    }
                },
                'postcss-loader',
                'sass-loader'
            ]
        }, {
            test  : /\.(png|jpg|woff|woff2|ttf|svg|eot)$/,
            loader: 'url-loader?limit=8192'
        }]
    }

};
